<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		//Read Session
		$logged = (isset($_SESSION['br_logged'])) ? $_SESSION['br_logged'] : false;

		//Check Session
		if ($logged) { redirect( base_url().'dashboard' ); }
	}

	public function index()
	{
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('forgot/index');
		$this->load->view('includes/footer');
	}
}
