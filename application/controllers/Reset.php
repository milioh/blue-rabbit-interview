<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reset extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		//Read Session
		$logged = (isset($_SESSION['br_logged'])) ? $_SESSION['br_logged'] : false;

		//Check Session
		if ($logged) { redirect( base_url().'dashboard' ); }
	}

	public function index()
	{
		//Read Values from URL
		$iduser = $this->uri->segment(2);
		$token =  $this->uri->segment(3);
		
		//Check if values are valids
		$array = array(
			'msg' => 'checkReset',
			'fields' => array(
				'iduser' => $iduser,
				'token' => $token
			),
			'app' => 'br_web',
			'apikey' => '08704d5fc31f838d8a0089830be4d4e7411893c0',
			'platform' => 'web'
		);
		$json_array = json_encode($array);
		
		//Request Call
		$response = $this->functions->call($json_array);
		$response_row = json_decode($response, true);
				
		//Check Request Call Status
		if ((int)$response_row['status'] == 1)
		{
			//Read Data from User
			$data['iduser'] = $iduser;
			
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('reset/index', $data);
			$this->load->view('includes/footer');
		}
		else
		{
			//Delete Session
			session_destroy();
	
			//Redirect to Login
			redirect( base_url() );
		}
	}
}
