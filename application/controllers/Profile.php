<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		//Read Session
		$logged = (isset($_SESSION['br_logged'])) ? $_SESSION['br_logged'] : false;
		
		//Check Session
		if (!$logged) { redirect( base_url() ); }
	}

	public function index()
	{
		//Read Id User
		$iduser = (isset($_SESSION['user']['iduser'])) ? (string)trim($_SESSION['user']['iduser']) : '';
		
		//Create JSON Request
		$array = array(
			'msg' => 'getUserInfo',
			'fields' => array(
				'iduser' => $iduser
			),
			'app' => 'br_web',
			'apikey' => '08704d5fc31f838d8a0089830be4d4e7411893c0',
			'platform' => 'web'
		);
		$json_array = json_encode($array);
		
		//Request Call
		$response = $this->functions->call($json_array);
		$response_row = json_decode($response, true);
				
		//Check Request Call Status
		if ((int)$response_row['status'] == 1)
		{
			//Read Data from User
			$data = $response_row['data'];
			
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('includes/navbar');
			$this->load->view('dashboard/index', $data);
			$this->load->view('includes/footer');
		}
		else
		{
			//Delete Session
			session_destroy();
	
			//Redirect to Login
			redirect( base_url() );
		}	
	}
	
}