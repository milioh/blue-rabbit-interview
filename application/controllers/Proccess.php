<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// How to send the response header with PHP
if (isset($_SERVER['HTTP_ORIGIN'])) {
		header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

		exit(0);
}

ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 10000);

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Common\Aws;
use Aws\Common\Enum\Size;
use Aws\S3\Enum\CannedAcl;
use Aws\Common\Enum\Region;
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\Common\Model\MultipartUpload\AbstractUploadBuilder;
use Abraham\TwitterOAuth\TwitterOAuth;

class Proccess extends CI_Controller {

	public $bucket = 'bluerabbit';

    private $_key = 'AKIAJJXOTGPPE4GLY5LA';

    private $_secret = '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';

    public $s3 = null;

	public function __construct()
	{
		parent::__construct();
		
		$this->s3 = S3Client::factory(array(
			'version' => '2006-03-01',
			'region' => 'us-west-2',
            'credentials' => array(
                'key'    => $this->_key,
                'secret' => $this->_secret,
            )
        ));
	}
	
	public function index()
	{
		//Redirect Home
		redirect(base_url(), 'refresh');
	}
	
	public function update_profile()
	{
		//Read Form Data
		$iduser = (isset($_POST['iduser'])) ? (string)trim($_POST['iduser']) : '';
		$first_name = (isset($_POST['inputFirstName'])) ? (string)trim($_POST['inputFirstName']) : '';
		$last_name = (isset($_POST['inputLastName'])) ? (string)trim($_POST['inputLastName']) : '';
		$email = (isset($_POST['inputEmail'])) ? (string)trim($_POST['inputEmail']) : '';
		$avatar = (isset($_POST['avatar'])) ? (string)trim($_POST['avatar']) : '';
		$language = (isset($_POST['selectLanguage'])) ? (string)trim($_POST['selectLanguage']) : '';
		$birth_date = (isset($_POST['actualDate'])) ? (string)trim($_POST['actualDate']) : '';
		$gender = (isset($_POST['selectGender'])) ? (string)trim($_POST['selectGender']) : '';
		$password = (isset($_POST['inputCurrentPassword'])) ? (string)trim($_POST['inputCurrentPassword']) : '';
		$new_password = (isset($_POST['inputNewCurrentPassword'])) ? (string)trim($_POST['inputNewCurrentPassword']) : '';
		$new_confirm_password = (isset($_POST['inputNewConfirmCurrentPassword'])) ? (string)trim($_POST['inputNewConfirmCurrentPassword']) : '';
		
		//Check Upload Image
		if(isset($_FILES['the_player_picture']['tmp_name']))
		{			
			if ($_FILES['the_player_picture']['tmp_name'] != '')
			{
				//Create File Path
				$filename = explode('.', $_FILES['the_player_picture']['name']);
				$id_array = count($filename) - 1;
				$key = time() . '_'.$iduser.'.' . strtolower($filename[$id_array]);
				$key = 'images/' . $key;
				$fullPath = $_FILES['the_player_picture']['tmp_name'];
				
				//Upload to S3
				$result = $this->s3->upload(
		            $this->bucket, //bucket
		            $key, //key, unique by each object
		            fopen($fullPath, 'rb'), //where is the file to upload?
		            'public-read' //permissions
		        );
				$avatar = $result['ObjectURL'];
				$avatar = str_replace('%2F', '/', $avatar);
				$avatar = str_replace('https://bluerabbit.s3.us-west-2.amazonaws.com', 'https://d33o2o8hwpd7uz.cloudfront.net', $avatar);
			}
		}
		
		//Create JSON Request
		$array = array(
			'msg' => 'updateProfile',
			'fields' => array(
				'iduser' => $iduser,
				'first_name' => $first_name,
				'last_name' => $last_name,
				'email' => $email,
				'avatar' => $avatar,
				'birth_date' => $birth_date,
				'gender' => $gender,
				'language' => $language,
				'password' => $password,
				'new_password' => $new_password,
				'new_confirm_password' => $new_confirm_password
			),
			'app' => 'br_web',
			'apikey' => '08704d5fc31f838d8a0089830be4d4e7411893c0',
			'platform' => 'web'
		);
		$json_array = json_encode($array);

		//Request Call
		$response = $this->functions->call($json_array);
		$response_row = json_decode($response, true);
		
		//Check Request Call Status
		if ((int)$response_row['status'] != 1)
		{
			//SESSION DATA
			$_SESSION['user'] = $data_result;
		}
		
		//Redirect Media
		redirect( base_url("dashboard") );
	}
	
	public function login_twitter()
	{
		// Twitter Keys
		$consumer_key = 'ZcPZXkBNgrzgaM2YZxI61YRza';
		$consumer_secret = 'GLOAWYoN8az4UB3EzzohhrthFZ5OmMKPaCVSwGwgbaAbIubvMs';
		
		// Twitter Connection
		$connection = new TwitterOAuth($consumer_key, $consumer_secret);
		$temporary_credentials = $connection->oauth('oauth/request_token', array( "oauth_callback" => base_url('proccess/twitter_data' ) ) );
		$_SESSION['oauth_token'] = $temporary_credentials['oauth_token'];       
		$_SESSION['oauth_token_secret'] = $temporary_credentials['oauth_token_secret'];
		$url = $connection->url("oauth/authorize", array("oauth_token" => $temporary_credentials['oauth_token']));
	
		redirect( $url );
	}
	
	public function twitter_data()
	{
		if(isset($_SESSION['oauth_token']))
		{
			//Read Token Generated
			$oauth_token = $_SESSION['oauth_token']; 
			unset($_SESSION['oauth_token']);
			
			//Credentials and Twitter Connection
			$consumer_key = 'ZcPZXkBNgrzgaM2YZxI61YRza';
			$consumer_secret = 'GLOAWYoN8az4UB3EzzohhrthFZ5OmMKPaCVSwGwgbaAbIubvMs';
			$connection = new TwitterOAuth($consumer_key, $consumer_secret);
			
			//Params from URL
			$params = array("oauth_verifier" => $_GET['oauth_verifier'],"oauth_token"=>$_GET['oauth_token']);
			
			//Generate Access Token
			$access_token = $connection->oauth("oauth/access_token", $params);
			$access_token_json = json_encode($access_token);
			
			//Get User Data
			$connection = new TwitterOAuth($consumer_key, $consumer_secret,$access_token['oauth_token'],$access_token['oauth_token_secret']);
			$content = $connection->get("account/verify_credentials", ["include_email" => true]);
			
			//Proccess Data
			$array_name = explode(' ', $content->name);
			$first_name = $array_name[0];
			$last_name = (count($array_name) > 1) ? (string)trim(str_replace($first_name, "", $content->name)) : '';
			
			//Create JSON Request
			$array = array(
				'msg' => 'twitterLogin',
				'fields' => array(
					'id' => $content->id,
					'first_name' => $first_name,
					'last_name' => $last_name,
					'screen_name' => $content->screen_name,
					'avatar' => $content->profile_image_url_https,
					'email' => $content->email,
					'token' => $access_token_json
				),
				'app' => 'br_web',
				'apikey' => '08704d5fc31f838d8a0089830be4d4e7411893c0',
				'platform' => 'web'
			);
			$json_array = json_encode($array);
			
			//Request Call
			$response = $this->functions->call($json_array);
			$response_row = json_decode($response, true);
					
			//Check Request Call Status
			if ((int)$response_row['status'] == 1)
			{
				//SESSION DATA
				$this->session->set_userdata('user', $response_row['data']);
				$this->session->set_userdata('br_logged', TRUE);
					
				//Redirect to Login
				redirect( base_url() );
			}
			else
			{
				//Delete Session
				session_destroy();
		
				//Redirect to Login
				redirect( base_url() );
			}
		}
		else
		{
			redirect( base_url() );
		}
	}
	
	public function connect_twitter()
	{
		// Twitter Keys
		$consumer_key = 'ZcPZXkBNgrzgaM2YZxI61YRza';
		$consumer_secret = 'GLOAWYoN8az4UB3EzzohhrthFZ5OmMKPaCVSwGwgbaAbIubvMs';
		
		// Twitter Connection
		$connection = new TwitterOAuth($consumer_key, $consumer_secret);
		$temporary_credentials = $connection->oauth('oauth/request_token', array( "oauth_callback" => base_url('proccess/twitter_connect' ) ) );
		$_SESSION['oauth_token'] = $temporary_credentials['oauth_token'];       
		$_SESSION['oauth_token_secret'] = $temporary_credentials['oauth_token_secret'];
		$url = $connection->url("oauth/authorize", array("oauth_token" => $temporary_credentials['oauth_token']));
	
		redirect( $url );
	}
	
	public function twitter_connect()
	{
		if(isset($_SESSION['oauth_token']))
		{
			//Read Token Generated
			$oauth_token = $_SESSION['oauth_token']; 
			unset($_SESSION['oauth_token']);
			
			//Credentials and Twitter Connection
			$consumer_key = 'ZcPZXkBNgrzgaM2YZxI61YRza';
			$consumer_secret = 'GLOAWYoN8az4UB3EzzohhrthFZ5OmMKPaCVSwGwgbaAbIubvMs';
			$connection = new TwitterOAuth($consumer_key, $consumer_secret);
			
			//Params from URL
			$params = array("oauth_verifier" => $_GET['oauth_verifier'],"oauth_token"=>$_GET['oauth_token']);
			
			//Generate Access Token
			$access_token = $connection->oauth("oauth/access_token", $params);
			$access_token_json = json_encode($access_token);
			
			//Get User Data
			$connection = new TwitterOAuth($consumer_key, $consumer_secret,$access_token['oauth_token'],$access_token['oauth_token_secret']);
			$content = $connection->get("account/verify_credentials", ["include_email" => true]);
			
			//Proccess Data
			$array_name = explode(' ', $content->name);
			$first_name = $array_name[0];
			$last_name = (count($array_name) > 1) ? (string)trim(str_replace($first_name, "", $content->name)) : '';
			
			//Create JSON Request
			$array = array(
				'msg' => 'twitterLogin',
				'fields' => array(
					'id' => $content->id,
					'first_name' => $first_name,
					'last_name' => $last_name,
					'screen_name' => $content->screen_name,
					'avatar' => $content->profile_image_url_https,
					'email' => $content->email,
					'token' => $access_token_json
				),
				'app' => 'br_web',
				'apikey' => '08704d5fc31f838d8a0089830be4d4e7411893c0',
				'platform' => 'web'
			);
			$json_array = json_encode($array);
			
			//Request Call
			$response = $this->functions->call($json_array);
			$response_row = json_decode($response, true);
					
			//Check Request Call Status
			if ((int)$response_row['status'] == 1)
			{
				//SESSION DATA
				$this->session->set_userdata('user', $response_row['data']);
				$this->session->set_userdata('br_logged', TRUE);
					
				//Redirect to Login
				redirect( base_url("dashboard") );
			}
			else
			{
				//Delete Session
				session_destroy();
		
				//Redirect to Login
				redirect( base_url() );
			}
		}
		else
		{
			redirect( base_url() );
		}
	}
	
	public function login_google()
	{
		/* Google App Client Id */
		define('CLIENT_ID', '314683027237-5kv9puth1efi29nh0f79sdlaikgq6it5.apps.googleusercontent.com');
		
		/* Google App Client Secret */
		define('CLIENT_SECRET', 'Acf7tHhgDz3D6cuWI7tI7mqn');
		
		/* Google App Redirect Url */
		define('CLIENT_REDIRECT_URL', base_url('proccess/google_data') );
		
		//LOGIN URL
		$login_url = 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me') . '&redirect_uri=' . urlencode(CLIENT_REDIRECT_URL) . '&response_type=code&client_id=' . CLIENT_ID . '&access_type=online';
		
		redirect( $login_url );
	}
	
	public function google_data()
	{
		//Set Variables
		$url = 'https://www.googleapis.com/oauth2/v4/token';	
		$client_id = '314683027237-5kv9puth1efi29nh0f79sdlaikgq6it5.apps.googleusercontent.com';
		$redirect_uri = base_url('proccess/google_data');
		$client_secret = 'Acf7tHhgDz3D6cuWI7tI7mqn';
		$code = (isset($_GET['code'])) ? (string)trim($_GET['code']) : '';
		
		//Check Code exists
		if ($code)
		{
			//Send Curl Post
			$curlPost = 'client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code='. $code . '&grant_type=authorization_code';
			$ch = curl_init();		
			curl_setopt($ch, CURLOPT_URL, $url);		
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			curl_setopt($ch, CURLOPT_POST, 1);		
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);	
			$data = json_decode(curl_exec($ch), true);
			$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);		
			if($http_code != 200) 
				throw new Exception('Error : Failed to receieve access token');
				
			// Access Token
			$access_token = (isset($data['access_token'])) ? (string)trim($data['access_token']) : '';
						
			//Check if Token exists
			if ($access_token)
			{
				//Send Curl Post
				$url = 'https://www.googleapis.com/plus/v1/people/me';			
				$ch = curl_init();		
				curl_setopt($ch, CURLOPT_URL, $url);		
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));
				$data = json_decode(curl_exec($ch), true);
				$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);		
				if($http_code != 200) 
					throw new Exception('Error : Failed to get user information');
				
				//Proccess Data
				$email = (isset($data['emails'][0]['value'])) ? (string)trim($data['emails'][0]['value']) : '';
				$id = (isset($data['id'])) ? (string)trim($data['id']) : '';
				$first_name = (isset($data['name']['givenName'])) ? (string)trim($data['name']['givenName']) : '';
				$last_name = (isset($data['name']['familyName'])) ? (string)trim($data['name']['familyName']) : '';
				$avatar = (isset($data['image']['url'])) ? (string)trim($data['image']['url']) : '';
				$array_avatar = explode('?', $avatar);
				$avatar = $array_avatar[0];
				
				//Create JSON Request
				$array = array(
					'msg' => 'googleLogin',
					'fields' => array(
						'id' => $id,
						'first_name' => $first_name,
						'last_name' => $last_name,
						'avatar' => $avatar,
						'email' => $email,
						'token' => $access_token
					),
					'app' => 'br_web',
					'apikey' => '08704d5fc31f838d8a0089830be4d4e7411893c0',
					'platform' => 'web'
				);
				$json_array = json_encode($array);
				
				//Request Call
				$response = $this->functions->call($json_array);
				$response_row = json_decode($response, true);
						
				//Check Request Call Status
				if ((int)$response_row['status'] == 1)
				{
					//SESSION DATA
					$this->session->set_userdata('user', $response_row['data']);
					$this->session->set_userdata('br_logged', TRUE);
						
					//Redirect to Login
					redirect( base_url() );
				}
				else
				{
					//Delete Session
					session_destroy();
			
					//Redirect to Login
					redirect( base_url() );
				}
			}
			else
			{
				//Delete Session
				session_destroy();
		
				//Redirect to Login
				redirect( base_url() );
			}
		}
		else
		{
			//Delete Session
			session_destroy();
	
			//Redirect to Login
			redirect( base_url() );
		}
	}
	
	public function connect_google()
	{
		/* Google App Client Id */
		define('CLIENT_ID', '314683027237-5kv9puth1efi29nh0f79sdlaikgq6it5.apps.googleusercontent.com');
		
		/* Google App Client Secret */
		define('CLIENT_SECRET', 'Acf7tHhgDz3D6cuWI7tI7mqn');
		
		/* Google App Redirect Url */
		define('CLIENT_REDIRECT_URL', base_url('proccess/google_connect') );
		
		//LOGIN URL
		$login_url = 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me') . '&redirect_uri=' . urlencode(CLIENT_REDIRECT_URL) . '&response_type=code&client_id=' . CLIENT_ID . '&access_type=online';
		
		redirect( $login_url );
	}
	
	public function google_connect()
	{
		//Set Variables
		$url = 'https://www.googleapis.com/oauth2/v4/token';	
		$client_id = '314683027237-5kv9puth1efi29nh0f79sdlaikgq6it5.apps.googleusercontent.com';
		$redirect_uri = base_url('proccess/google_connect');
		$client_secret = 'Acf7tHhgDz3D6cuWI7tI7mqn';
		$code = (isset($_GET['code'])) ? (string)trim($_GET['code']) : '';
		
		//Check Code exists
		if ($code)
		{
			//Send Curl Post
			$curlPost = 'client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code='. $code . '&grant_type=authorization_code';
			$ch = curl_init();		
			curl_setopt($ch, CURLOPT_URL, $url);		
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			curl_setopt($ch, CURLOPT_POST, 1);		
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);	
			$data = json_decode(curl_exec($ch), true);
			$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);		
			if($http_code != 200) 
				throw new Exception('Error : Failed to receieve access token');
				
			// Access Token
			$access_token = (isset($data['access_token'])) ? (string)trim($data['access_token']) : '';
						
			//Check if Token exists
			if ($access_token)
			{
				//Send Curl Post
				$url = 'https://www.googleapis.com/plus/v1/people/me';			
				$ch = curl_init();		
				curl_setopt($ch, CURLOPT_URL, $url);		
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));
				$data = json_decode(curl_exec($ch), true);
				$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);		
				if($http_code != 200) 
					throw new Exception('Error : Failed to get user information');
				
				//Proccess Data
				$email = (isset($data['emails'][0]['value'])) ? (string)trim($data['emails'][0]['value']) : '';
				$id = (isset($data['id'])) ? (string)trim($data['id']) : '';
				$first_name = (isset($data['name']['givenName'])) ? (string)trim($data['name']['givenName']) : '';
				$last_name = (isset($data['name']['familyName'])) ? (string)trim($data['name']['familyName']) : '';
				$avatar = (isset($data['image']['url'])) ? (string)trim($data['image']['url']) : '';
				$array_avatar = explode('?', $avatar);
				$avatar = $array_avatar[0];
				
				//Create JSON Request
				$array = array(
					'msg' => 'googleLogin',
					'fields' => array(
						'id' => $id,
						'first_name' => $first_name,
						'last_name' => $last_name,
						'avatar' => $avatar,
						'email' => $email,
						'token' => $access_token
					),
					'app' => 'br_web',
					'apikey' => '08704d5fc31f838d8a0089830be4d4e7411893c0',
					'platform' => 'web'
				);
				$json_array = json_encode($array);
				
				//Request Call
				$response = $this->functions->call($json_array);
				$response_row = json_decode($response, true);
						
				//Check Request Call Status
				if ((int)$response_row['status'] == 1)
				{
					//SESSION DATA
					$this->session->set_userdata('user', $response_row['data']);
					$this->session->set_userdata('br_logged', TRUE);
						
					//Redirect to Login
					redirect( base_url("dashboard") );
				}
				else
				{
					//Delete Session
					session_destroy();
			
					//Redirect to Login
					redirect( base_url() );
				}
			}
			else
			{
				//Delete Session
				session_destroy();
		
				//Redirect to Login
				redirect( base_url() );
			}
		}
		else
		{
			//Delete Session
			session_destroy();
	
			//Redirect to Login
			redirect( base_url() );
		}
	}
}
