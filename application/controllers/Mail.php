<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		//Redirect to home
		redirect( site_url() );
	}
	
	public function createUser()
	{
		//Demo Array
		$data = array(
			'iduser' => '123',
			'email' => 'gauden@bluerabbit.io',
			'password' => 'blueRABBIT',
			'firstName' => 'Bernardo',
			'lastName' => 'Letayf',
			'idprofile' => '3',
		    'profile' => 'User'
		);
		
		//Load View
		$this->load->view('mail/createUser', $data);
	}
	
	public function forgotPassword()
	{
		//Demo Array
		$data = array(
			'iduser' => '123',
			'email' => 'gauden@bluerabbit.io',
			'firstName' => 'Bernardo',
			'lastName' => 'Letayf',
			'link' => base_url('reset/123/token')
		);
		
		//Load View
		$this->load->view('mail/forgotPassword', $data);
	}
}
