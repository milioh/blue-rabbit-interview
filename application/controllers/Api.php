<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		error_reporting(E_ALL);
		ini_set("display_errors", 1);
	}

	public function index()
	{
		//Read Params from Form
		$json = (isset($_POST['param'])) ? $_POST['param'] : NULL;
		if (!$json) { $json = file_get_contents("php://input"); }
		
		//Check Params exists
		if ($json != NULL)
		{
			//Decode JSON to Array
			$json_decode = json_decode($json, true);

			//Read Message Value from JSON
			$msg = (isset($json_decode['msg'])) ? $json_decode['msg'] : '';

			//Read Fields Value from JSON
			$fields = (isset($json_decode['fields'])) ? $json_decode['fields'] : '';

			//Read App Value from JSON
			$app = (isset($json_decode['app'])) ? $json_decode['app'] : '';

			//Read ApiKey Value from JSON
			$apikey = (isset($json_decode['apikey'])) ? $json_decode['apikey'] : '';
			
			//Read Platform Value from JSON
			$platform = (isset($json_decode['platform'])) ? $json_decode['platform'] : 'web';

			//Execute JSON
			$this->engine->executeJSON($msg,$fields,$app,$apikey,$platform);
		}
		else
		{
			//Redirect API Home
			$this->load->view('api/index');
		}
	}
}