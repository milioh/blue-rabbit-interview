
		<div class="login-bg">
			<div class="image"></div>
			<div class="pattern-overlay"></div>
			<div class="color-overlay"></div>
		</div>
		<div class="br-container boxed login">
			<div class="br-row">
				<div class="br-col-12">
					<div class="crossed-text">
						<h5>Type your new Password</h5>
					</div>
				</div>
			</div>
			<div class="br-row"> 
				<div class="br-col-6">
					<div class="logo"><img src="<?php echo base_url("assets/images/logo-white-letters.png"); ?>" width="367" alt=""/></div>
				</div>
				<div class="br-col-6">
					<form name="resetform" id="resetform" action="" method="post">
						<input type="hidden" id="iduser" name="iduser" value="<?php echo $iduser; ?>" />
						<div class="br-input-group">
							<label class="blue">New Password</label>
							<input class="br-form-control" id="reset-new-password" name="inputNewPassword" type="password">
						</div>
						<div class="br-input-group">
							<label class="blue">Confirm New Password</label>
							<input class="br-form-control" id="reset-new-confirm-password" name="inputNewConfirmPassword" type="password">
						</div>
						<button id="submit-resetform" name="submit-resetform" type="submit" class="br-form-control big blue">Change Password</button>
					</form>
				</div>
			</div>
		</div>