
		<div class="main-content">
			<div class="theme blue"> 
				<div class="br-container">
					<div class="br-header theme-border">
						<h1 class="theme-color">
							<span class="icon icon-profile"></span>
							My Account
						</h1>
					</div>
					<form name="profileform" id="profileform" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url("proccess/update_profile"); ?>" method="post">
						<input type="hidden" id="iduser" name="iduser" value="<?php echo $iduser; ?>" />
						<div class="br-profile-sidebar">
							<div class="br-profile-badge">
								<div class="badge-select-overlay">
									<button id="btnUploadPicture" name="btnUploadPicture" class="br-form-control">Upload Picture</button>
								</div>
								<div class="br-badge-img uploaded-image" id="the_player_picture_thumb" style="background-image: url(<?php echo $avatar; ?>)">
									<input type="file" style="position: absolute; margin-top: -9999px;" id="the_player_picture" name="the_player_picture" value=""/>
									<input type="hidden" id="avatar" name="avatar" value="<?php echo $avatar; ?>" />
								</div>
							</div>
						</div>
						<div class="br-content br-profile-content">
							<div class="br-sub-header theme-border">
								<h1 class="theme-color">
									Player Info
								</h1>
							</div>
							<div class="br-row">
								<div class="br-col-6">
									<div class="br-input-group">
										<label class="pink">Nombre</label>
										<input class="br-form-control" id="the_first_name" name="inputFirstName" type="text" value="<?php echo $firstName; ?>" >
									</div>
								</div>
								<div class="br-col-6">
									<div class="br-input-group">
										<label class="pink">Apellido</label>
										<input class="br-form-control" id="the_last_name" name="inputLastName" type="text" value="<?php echo $lastName; ?>" >
									</div>
								</div>
							</div>
							<div class="br-row">
								<div class="br-col-12">
									<div class="br-input-group">
										<label class="blue">@ Email</label>
										<input class="br-form-control" type="text" id="the_email" name="inputEmail" value="<?php echo $email; ?>">
									</div>
								</div>
							</div>
							<div class="br-row">
								<div class="br-col-4">
									<div class="br-input-group">
										<label class="green"><span class="icon icon-language"></span>Language</label>
										<select class="br-form-control" id="the_lang" name="selectLanguage">
											<option value="en_US"<?=($language == 'en_US') ? ' selected' : ''; ?>>U.S. English</option>
											<option value="es_MX"<?=($language == 'es_MX') ? ' selected' : ''; ?>>Español</option>
											<option value="tr_TR"<?=($language == 'tr_TR') ? ' selected' : ''; ?>>Türk</option>
										</select>
									</div>
								</div>
								<div class="br-col-4">
									<div class="br-input-group">
										<?php 
											if (isset($birthDate)) 
											{ 
												if ($birthDate)
												{
													$array_date = explode('-', $birthDate); 
													$birth_date = $array_date[2] . '/' . $array_date[1] . '/' . $array_date[0]; 
												}
												else
												{
													$birth_date = ''; 
												}
											} 
											else 
											{ 
												$birth_date = ''; 
											} 
										?> 
										<label class="cyan"><span class="icon icon-calendar"></span> Birthdate</label>
										<input class="br-form-control" id="the_player_brithdate" name="inputBirthDate" type="text" value="<?php echo $birth_date; ?>" >
										<input type="hidden" id="actualDate" name="actualDate" value="<?php echo $birthDate; ?>" />
									</div>
								</div>
								<div class="br-col-4">
									<div class="br-input-group">
										<label class="teal"><span class="icon icon-profile"></span>Gender</label>
										<select class="br-form-control" id="the_player_gender" name="selectGender">
											<option value="">-Select-</option>
											<option value="m"<?=($gender == 'm') ? ' selected' : ''; ?>>Male</option>
											<option value="f"<?=($gender == 'f') ? ' selected' : ''; ?>>Female</option>
											<option value="o"<?=($gender == 'o') ? ' selected' : ''; ?>>Other</option>
										</select>
									</div>
								</div>
							</div>
							<?php
								//Show Password
								$show_password = true;
								
								//Set Twitter Token Array
								$twitter_token = array(); $screen_name = '';
								//Check if twToken exists
								if (isset($twToken))
								{
									//Decode JSON
									$twitter_token = json_decode($twToken);
									
									//Read Screen Name
									$screen_name = $twitter_token->screen_name;
									
									$show_password = false;
								}	
								
								//Set Google Token
								$google_token = '';
								//Check if twToken exists
								if (isset($gaToken))
								{
									//Decode JSON
									$google_token = (string)trim($gaToken);
									
									if ($google_token) $show_password = false;
								}	
							?>
							<div class="br-row">
								<div class="br-col-6">
									<?php if ($screen_name) { ?>
									<button class="br-form-control blue" disabled="disabled"><span class="icon icon-tw"></span> Conectado como @<?php echo $screen_name; ?></button>
									<?php } else { ?>
									<button id="btnTwitterConnect" class="br-form-control blue"><span class="icon icon-tw"></span> Conectar con Twitter</button>
									<?php } ?>
								</div>
								<div class="br-col-6">
									<?php if ($google_token) { ?>
									<button class="br-form-control red" disabled="disabled"><span class="icon icon-ga"></span> Conectado con tu correo de Google</button>
									<?php } else { ?>
									<button id="btnGoogleConnect" class="br-form-control red"><span class="icon icon-ga"></span> Conectar con Google</button>
									<?php } ?>
								</div>
								<input type="hidden" id="the_timezone" value="MX">
							</div>
							<?php if ($show_password) { ?>
							<div class="divider thin long"></div> 
							<div class="br-row">
								<div class="br-col-4">
									<div class="br-input-group">
										<label class="orange"><span class="icon icon-key"></span>Current password</label>
										<input class="br-form-control" id="current-password" name="inputCurrentPassword" type="password" value="" >
									</div>
								</div>
								<div class="br-col-4">
									<div class="br-input-group">
										<label class="deep-orange"><span class="icon icon-key"></span>New password</label>
										<input class="br-form-control" id="new-current-password" name="inputNewCurrentPassword" type="password" value="" >
									</div>
								</div>
								<div class="br-col-4">
									<div class="br-input-group">
										<label class="deep-orange"><span class="icon icon-key"></span>Confirm New password</label>
										<input class="br-form-control" id="new-confirm-current-password" name="inputNewConfirmCurrentPassword" type="password" value="" >
									</div>
								</div>
							</div>
							<?php } ?>
							<div class="divider thin long"></div> 
							<div class="br-row">
								<button id="submit-profileform" name="submit-profileform" class="br-form-control big green pull-right">Save <span class="icon icon-arrow-right"></span></button>
							</div>
							<?php $form_error = (isset($_SESSION['form_error'])) ? (string)trim($_SESSION['form_error']) : ''; ?>
							<?php if ($form_error) { ?>
							<div class="br-row">
								<h4 class="theme-color">
									<?php echo $form_error; $_SESSION['form_error'] = ''; ?>
								</h4>
							</div>
							<?php } ?>
							<div class="divider thin long"></div>
						</div>
					</form>
				</div>
			</div>
		</div>