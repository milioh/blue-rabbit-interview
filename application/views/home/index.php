		
		<div class="login-bg">
			<div class="image"></div>
			<div class="pattern-overlay"></div>
			<div class="color-overlay"></div>
		</div>
		<div class="register-now">
			<a href="#registration"><img src="<?php echo base_url("assets/images/register.png"); ?>" width="182" alt="" /> </a>
		</div>
		<div class="br-container boxed login">
			<div class="br-row"> 
				<div class="br-col-6">
					<div class="logo"><img src="<?php echo base_url("assets/images/logo-white-letters.png"); ?>" width="367" alt=""/></div>
				</div>
				<div class="br-col-6">
					<form name="loginform" id="loginform" action="" method="post">
						<div class="br-input-group">
							<label class="blue">Email</label>
							<input class="br-form-control" id="login-user-name" name="inputEmail" type="text">
						</div>
						<div class="br-input-group">
							<label class="blue">Password</label>
							<input class="br-form-control" id="login-password" name="inputPassword" type="password">
						</div>
						<div class="br-input-group">
							<div class="g-recaptcha" data-sitekey="6Lf6_mwUAAAAACCxokilZaXUPAAH6RuojWKMEwkm"></div>
						</div>
						<button id="submit-loginform" name="submit-loginform" type="submit" class="br-form-control big blue"><span class="icon icon-sign"></span>Login</button>
					</form>
				</div>
				<div class="col-xs-12 forgot-password">
					<a href="<?php echo base_url("forgot"); ?>">Forgot password?</a>
				</div>
			</div>
			<div class="br-row"> 
				<div class="separator"></div>
				<div class="br-col-6 text-center">
					<button id="btnTwitterLogin" type="button" class="br-form-control big blue"><span class="icon icon-tw"></span> Login with Twitter</button>
				</div>
				<div class="br-col-6 text-center">
					<button id="btnGoogleLogin" type="button" class="br-form-control big red">Login with Google</button>
				</div>
				<div class="separator"></div>
			</div>
			<div class="br-row"> 
				<div class="br-col-12" id="registration">
					<form name="registerform" id="registerform" action="" method="post">
						<div class="crossed-text">
							<h5>Register for free!</h5>
						</div>
						<div class="br-container">
							<div class="br-row">
								<div class="br-col-6">
									<div class="br-input-group">
										<label class="grey">Email</label>
										<input class="br-form-control" id="user_email" name="registerInputEmail" type="text">
									</div>
								</div>
								<div class="br-col-6">
									<div class="br-input-group">
										<label class="grey">Password</label>
										<input class="br-form-control" id="password" name="registerInputPassword" type="password">
									</div>
								</div>
								<div class="br-col-4">
									<div class="br-input-group">
										<label class="blue">First name</label>
										<input class="br-form-control" id="user_name" name="registerInputFirstName" type="text">
									</div>
								</div>
								<div class="br-col-4">
									<div class="br-input-group">
										<label class="purple">Last name(s)</label>
										<input class="br-form-control" id="user_lastname" name="registerInputLastName" type="text">
									</div>
								</div>
								<div class="br-col-4">
									<div class="br-input-group">
										<label class="green"><span class="icon icon-language"></span>Language</label>
										<select class="br-form-control" id="the_lang" name="registerSelectLanguage">
											<option value="en_US">U.S. English</option>
											<option selected="" value="es_MX">Español</option>
											<option value="tr_TR">Türk</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="login-button">
							<br class="clear">
							<button id="submit-registerform" name="submit-registerform" class="br-form-control big blue"><span class="icon icon-sign"></span>Register New Player</button>
						</div>
						<p>By registering you agree upon our <a class="solid-blue" target="_blank" href="http://www.bluerabbit.io/privacy"> privacy policy and terms of service</a></p>
						<div class="divider"></div>
					</form>
				</div>
			</div>
		</div>