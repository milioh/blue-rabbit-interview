
		<div class="login-bg">
			<div class="image"></div>
			<div class="pattern-overlay"></div>
			<div class="color-overlay"></div>
		</div>
		<div class="br-container boxed login">
			<div class="br-row">
				<div class="br-col-12">
					<div class="crossed-text">
						<h5>Type your Email and Follow the Instructions</h5>
					</div>
				</div>
			</div>
			<div class="br-row"> 
				<div class="br-col-6">
					<div class="logo"><img src="<?php echo base_url("assets/images/logo-white-letters.png"); ?>" width="367" alt=""/></div>
				</div>
				<div class="br-col-6">
					<form name="forgotform" id="forgotform" action="" method="post">
						<div class="br-input-group">
							<label class="blue">Email</label>
							<input class="br-form-control" id="forgot-user-name" name="inputEmail" type="text">
						</div>
						<button id="submit-forgotform" name="submit-forgotform" type="submit" class="br-form-control big blue">Reset Password</button>
					</form>
				</div>
			</div>
		</div>