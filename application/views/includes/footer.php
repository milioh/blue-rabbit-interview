	
		<div class="footer" id="footer">
			<div class="">
				<ul class="footer-menu">
					<li><a href="">My Classes</a></li>
					<li><a href="http://www.bluerabbit.io/blog" target="_blank">Blog</a></li>
					<li><a href="http://www.bluerabbit.io/credits" target="_blank">Credits</a></li>
					<li><a href="http://www.bluerabbit.io/docs" target="_blank">Documentation</a></li>
					<li><a href="http://www.bluerabbit.io/contact" target="_blank">Contact</a></li>
					<li><a href="http://bluerabbit.io/privacy">Privacy</a></li>

					<li><a href="http://www.bluerabbit.io" target="_blank">Bluerabbit.io</a></li>
				</ul>
			</div>
		</div>
	
		<input type="hidden" id="base_url" name="base_url" value="<?php echo base_url(); ?>" />
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url("assets/js/jquery-ui.min.js"); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url("assets/js/validation/jquery.validate.min.js"); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url("assets/js/validation/additional-methods.min.js"); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url("assets/js/validation/localization/messages_es.min.js"); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url("assets/js/jquery.form.min.js"); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url("assets/js/functions.js?ver=1.0.41"); ?>"></script>
	
	</body>
</html>