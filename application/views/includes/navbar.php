		
		<?php 
			//Read Variables from Session
			$slug = $this->uri->segment(1, 0);
			$firstName = (isset($_SESSION['user']['firstName'])) ? (string)trim($_SESSION['user']['firstName']) : '';
			$lastName = (isset($_SESSION['user']['lastName'])) ? (string)trim($_SESSION['user']['lastName']) : ''; 
		?>
		<div class="super-header" id="super-header">
			<div class="class-title">
				<span class="icon icon-logo"></span>
				Welcome to BLUErabbit
			</div>
			<div class="basic-status">
				<div class="player-name">
					<h3> <a href="<?php echo base_url("logout"); ?>"><?php echo $firstName; ?></a></h3>
					<h6> <a href="<?php echo base_url("logout"); ?>"><?php echo $lastName; ?></a></h6>
				</div>
			</div>
		</div>