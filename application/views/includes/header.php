<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>
			BLUErabbit
		</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<link rel="icon" type="image/gif" href="<?php echo base_url("assets/images/favicon.gif"); ?>">
		<link rel="alternate" href="http://bluerabbit.io" hreflang="en" />

		<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/jquery-ui.min.css"); ?>" >
		<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/style.css?ver=1.0.5"); ?>">
		<link rel="stylesheet" href="https://use.typekit.net/zfu4fjz.css">
		
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
		
	</head>
	<body>