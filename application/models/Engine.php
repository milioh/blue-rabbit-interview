<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// How to send the response header with PHP
if (isset($_SERVER['HTTP_ORIGIN'])) {
		header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

		exit(0);
}

// PHP Variables
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 10000);

use PHPMailer\PHPMailer\PHPMailer; 
use PHPMailer\PHPMailer\Exception;

class Engine extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	//Function to Indent JSON when send Response
	public function indent($json)
	{
	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen($json);
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;

	    for ($i=0; $i<=$strLen; $i++) {

	        // Grab the next character in the string.
	        $char = substr($json, $i, 1);

	        // Are we inside a quoted string?
	        if ($char == '"' && $prevChar != '\\') {
	            $outOfQuotes = !$outOfQuotes;

	        // If this character is the end of an element,
	        // output a new line and indent the next line.
	        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
	            $result .= $newLine;
	            $pos --;
	            for ($j=0; $j<$pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        // Add the character to the result string.
	        $result .= $char;

	        // If the last character was the beginning of an element,
	        // output a new line and indent the next line.
	        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
	            $result .= $newLine;
	            if ($char == '{' || $char == '[') {
	                $pos ++;
	            }

	            for ($j = 0; $j < $pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        $prevChar = $char;
	    }

	    return $result;
	}

	//Function to Print JSON as a Response
	public function printJSON($array)
	{
		$json = json_encode($array);
		header('Content-Type: application/json',true);
		echo $this->indent($json);
	}
	
	//Message Controller
	public function executeJSON($msg,$fields,$app,$apikey,$platform)
	{
		//Read Configuration
		$output = FALSE;
		$array = array();
		$origin = (isset($_SERVER['HTTP_ORIGIN'])) ? (string)trim($_SERVER['HTTP_ORIGIN']) : '';
		$ip = (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) ? (string)trim($_SERVER['HTTP_X_FORWARDED_FOR']) : '';
		$valid_origins = array(
			'http://interview.bluerabbit.io',
			'http://dev.bluerabbit.io',
			'http://localhost:8100',
			'http://localhost:8080',
			'file://'
		);
		
		//Check Origin to give access to execute
		if (in_array($origin, $valid_origins))
		{
			switch ($platform) {
				case 'web': $app = 'br_web'; $apikey = '08704d5fc31f838d8a0089830be4d4e7411893c0'; break;
				case 'ios': $app = 'br_ios'; $apikey = '32a90dcab6f12db9fe0ffe56eaf98d10be60f024'; break;
				case 'android': $app = 'br_android'; $apikey = '5efe57ca263366005cbe6561d0fc8f9293996ab2'; break;
			}
		}
		
		//Query API User
		$query = "SELECT * FROM app WHERE app = '" . $app . "' AND apikey = '" . $apikey . "' AND status = 1 LIMIT 1";
		$query_app = $this->db->query("SELECT * FROM app WHERE app = '" . $app . "' AND apikey = '" . $apikey . "' AND status = 1 LIMIT 1");
		
		//Check if the API User exists
		if ($query_app->num_rows() > 0)
		{
			//Read App Object
			$row_app = $query_app->row();
			$calls = explode(',', $row_app->permissions);
			
			//Check apps from Blue Rabbit
			if ($row_app->idapp > 3)
			{
				//Check Valid Origins
				if (!in_array($origin, $valid_origins))	
				{
					//Send Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)"You don't have permission to execute this method. Ask permission to the Admin."
					);

					//Print JSON Response
					$this->printJSON($array);
					$output = TRUE;
					
					//Save Log to Database
					$this->saveLog($msg,$fields,$app,$array);
					
					die();
				}
			}
			
			// getUserInfo
			if ($msg == 'getUserInfo' && in_array('getUserInfo', $calls))
			{
				//Read Fields
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				
				//Check Values
				if ($iduser)
				{
					//Query User Info
					$query = $this->db->query("SELECT * FROM user WHERE iduser = '" . $iduser . "' LIMIT 1");
	
					//Check if user exists
					if ($query->num_rows() > 0)
					{
						//Read Object
						$row_user = $query->row();
						
						//Query Profile
						$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $row_user->idprofile . " AND status = 1");
						$row_profile = $query_profile->row();
						
						//Generate Array to Respond via JSON
						$data_result = array(
							'iduser' => $row_user->iduser,
							'firstName' => $row_user->firstName,
							'lastName' => $row_user->lastName,
							'email' => $row_user->email,
							'avatar' => $row_user->avatar,
							'birthDate' => $row_user->birthDate,
							'gender' => $row_user->gender,
							'language' => $row_user->language,
							'twId' => $row_user->twId,
							'twToken' => $row_user->twToken,
							'gaId' => $row_user->gaId,
							'gaToken' => $row_user->gaToken,
							'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name
						);
						
						//Show Error
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data_result
						);
	
						//Print JSON Response
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Show Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Este usuario no existe.',
							'data' => array()
						);
	
						//Print JSON Response
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'fields' => $fields
					);
	
					//Print JSON Response
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// doLogin
			if ($msg == 'doLogin' && in_array('doLogin', $calls))
			{
				//Read Fields
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
				
				//Check Values
				if ($email && $password)
				{
					//Query if the email exists already
					$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' LIMIT 1");
	
					//Check if user exists
					if ($query->num_rows() > 0)
					{
						//Read Object
						$row = $query->row();
						
						//Read Password Hash
						$hash = $row->password;
						
						//Check Password 
						if (password_verify($password, $hash)) 
						{
							//Query Profile
							$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $row->idprofile . " AND status = 1");
							$row_profile = $query_profile->row();
						
							//Generate Array to Respond via JSON
							$data_result = array(
								'iduser' => $row->iduser,
								'firstName' => $row->firstName,
								'lastName' => $row->lastName,
								'email' => $row->email,
								'avatar' => $row->avatar,
								'idprofile' => $row_profile->idprofile,
							    'profile' => $row_profile->name
							);
							
							//SESSION DATA
							$this->session->set_userdata('user', $data_result);
							$this->session->set_userdata('br_logged', TRUE);
		
							//Create Array Response
							$array = array(
								'status' => (int)1,
								'msg' => 'success',
								'data' => $data_result
							);
		
							//Print JSON Response
							$this->printJSON($array);
							$output = TRUE;
						}
						else
						{
							//Show Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'Contraseña incorrecta. Intenta de nuevo.'
							);
		
							//Print JSON Response
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Show Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Correo Electrónico no existe. Intenta de nuevo.'
						);
	
						////Print JSON Response
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'fields' => $fields
					);
	
					//Print JSON Response
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// createUser
			if ($msg == 'createUser' && in_array('createUser', $calls))
			{
				//Read Fields
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
				$first_name = (isset($fields['first_name'])) ? (string)trim($fields['first_name']) : '';
				$last_name = (isset($fields['last_name'])) ? (string)trim($fields['last_name']) : '';
				$language = (isset($fields['language'])) ? (string)trim($fields['language']) : '';
				$idprofile = 3;
				
				//Check Values
				if ($email && $password && $first_name && $last_name && $language)
				{
					//Query if the email exists already
					$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' LIMIT 1");
	
					//Check if user exists
					if ($query->num_rows() == 0)
					{
						//Save data into users
						$data = array(
							'firstName' => $first_name,
							'lastName' => $last_name,
							'email' => $email,
							'password' => password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]),
							'language' => $language,
						    'idprofile' => $idprofile,
						    'createdAt' => date('Y-m-d H:i:s'),
						    'status' => 1
						);
						$this->db->insert('user', $data);
						$iduser = $this->db->insert_id();
						
						//Query Profile
						$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $idprofile . " AND status = 1");
						$row_profile = $query_profile->row();
						
						//Generate Data to send by Mail
						$data = array(
							'iduser' => $iduser,
							'email' => $email,
							'password' => $password,
							'firstName' => $first_name,
							'lastName' => $last_name,
							'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name
						);
						
						//Generate Array to Respond via JSON
						$data_result = array(
							'iduser' => $iduser,
							'firstName' => $first_name,
							'lastName' => $last_name,
							'email' => $email,
							'avatar' => '',
							'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name
						);
						
						//SESSION DATA
						$this->session->set_userdata('user', $data_result);
						$this->session->set_userdata('br_logged', TRUE);
	
						//Send Welcome Mail to blueRABBIT
						$subject = 'Welcome to blueRABBIT';
						$body = $this->load->view('mail/createUser', $data, TRUE);
						$altbody = strip_tags($body);
	
						//Check Data to send Mail
						if ($subject != '' && $body != '' && $altbody != '' && $email != '')
						{
							//Send Email
							$mail = new PHPMailer(true);
							$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
							$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
							$mail->CharSet = 'UTF-8';
							$mail->Encoding = "quoted-printable";
					        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
					        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
					        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
					        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
					        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
					        $mail->Username   = "AKIAJK4S2TODUTBKQ65Q";  // la cuenta de correo GMail
					        $mail->Password   = "AlXW+LESj55ocLzrg5pZS8dqlQmIYSH/QZaft8uPCmOl";            // password de la cuenta GMail
					        $mail->SMTPDebug  = 0;
					        $mail->SetFrom("interview.bluerabbit@gmail.com", utf8_encode('blueRABBIT'));  //Quien envía el correo
					        $mail->AddReplyTo("interview.bluerabbit@gmail.com", utf8_encode("blueRABBIT"));  //A quien debe ir dirigida la respuesta
					        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
					        $mail->Body      = $body;
					        $mail->AltBody    = $altbody;
					        $mail->AddAddress($email, $first_name . ' ' . $last_name);
					        $mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
	
					        if ($mail->Send())
							{
								$response = true;
							}
							else
							{
								$response = false;
							}
						}
	
						//Create Array Response
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data_result
						);
	
						//Print JSON Response
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Show Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Este correo electrónico ya esta registrado.',
							'data' => array()
						);
	
						//Print JSON Response
						$this->printJSON($array);
						$output = TRUE;
					}
				} 
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'fields' => $fields
					);
	
					//Print JSON Response
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// updateProfile
			if ($msg == 'updateProfile' && in_array('updateProfile', $calls))
			{
				//Read Fields
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				$first_name = (isset($fields['first_name'])) ? (string)trim($fields['first_name']) : '';
				$last_name = (isset($fields['last_name'])) ? (string)trim($fields['last_name']) : '';
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				$avatar = (isset($fields['avatar'])) ? (string)trim($fields['avatar']) : '';
				$birth_date = (isset($fields['birth_date'])) ? (string)trim($fields['birth_date']) : '';
				$gender = (isset($fields['gender'])) ? (string)trim($fields['gender']) : '';
				$language = (isset($fields['language'])) ? (string)trim($fields['language']) : '';
				$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
				$new_password = (isset($fields['new_password'])) ? (string)trim($fields['new_password']) : '';
				$new_confirm_password = (isset($fields['new_confirm_password'])) ? (string)trim($fields['new_confirm_password']) : '';
				$password_error = '';
				
				//Check Values
				if ($iduser)
				{
					//Query if the email exists already
					$query = $this->db->query("SELECT * FROM user WHERE iduser = '" . $iduser . "' AND status = 1 LIMIT 1");
	
					//Check if user exists
					if ($query->num_rows() > 0)
					{
						//Read Object
						$row = $query->row();
						
						//Update data into users
						$data = array(
							'firstName' => $first_name,
							'lastName' => $last_name,
							'email' => $email,
							'avatar' => $avatar,
							'birthDate' => $birth_date,
							'gender' => $gender,
							'language' => $language,
						    'updatedAt' => date('Y-m-d H:i:s')
						);
						
						//Read Password Hash
						$hash = $row->password;
						
						//Check Current Password 
						if (password_verify($password, $hash)) 
						{
							//Check New Password 
							if ($new_password == $new_confirm_password)
							{
								//Add New Password value into the Data Array
								$data['password'] = password_hash($new_password, PASSWORD_DEFAULT, ['cost' => 12]);
							}
							else
							{
								//Create Password Error String
								$password_error = 'Debes de confirmar la nueva contraseña correctamente.';
							}
						}
						
						//Update Data from User
						$this->db->where('iduser', $iduser);
						$this->db->update('user', $data);
						
						//Query Profile
						$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $row->idprofile . " AND status = 1");
						$row_profile = $query_profile->row();
						
						//Generate Array to Respond via JSON
						$data_result = array(
							'iduser' => $iduser,
							'firstName' => $first_name,
							'lastName' => $last_name,
							'email' => $email,
							'avatar' => $avatar,
							'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name
						);
						
						//Create Array Response
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data_result
						);
	
						//Print JSON Response
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Show Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Este usuario no esta registrado.',
							'data' => array()
						);
	
						//Print JSON Response
						$this->printJSON($array);
						$output = TRUE;
					}
				} 
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'fields' => $fields
					);
	
					//Print JSON Response
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// forgotPassword
			if ($msg == 'forgotPassword' && in_array('forgotPassword', $calls))
			{
				//Read Fields
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				
				//Check Values
				if ($email)
				{
					//Query if the email exists already
					$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' LIMIT 1");
	
					//Check if user exists
					if ($query->num_rows() > 0)
					{
						//Read Object
						$row = $query->row();
						
						//Generate Token
						$token = sha1(now());
						
						//Update Token into User
						$data['token'] = $token;
						$this->db->where('iduser', $row->iduser);
						$this->db->update('user', $data);
						
						//Generate Data to send by Mail
						$data = array(
							'iduser' => $row->iduser,
							'email' => $row->email,
							'firstName' => $row->firstName,
							'lastName' => $row->lastName,
							'link' => base_url('reset/'.$row->iduser.'/'.$token)
						);
						
						//Send Welcome Mail to blueRABBIT
						$subject = 'Reset Password blueRABBIT';
						$body = $this->load->view('mail/resetPassword', $data, TRUE);
						$altbody = strip_tags($body);
	
						//Check Data to send Mail
						if ($subject != '' && $body != '' && $altbody != '' && $email != '')
						{
							//Send Email
							$mail = new PHPMailer(true);
							$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
							$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
							$mail->CharSet = 'UTF-8';
							$mail->Encoding = "quoted-printable";
					        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
					        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
					        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
					        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
					        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
					        $mail->Username   = "AKIAJK4S2TODUTBKQ65Q";  // la cuenta de correo GMail
					        $mail->Password   = "AlXW+LESj55ocLzrg5pZS8dqlQmIYSH/QZaft8uPCmOl";            // password de la cuenta GMail
					        $mail->SMTPDebug  = 0;
					        $mail->SetFrom("interview.bluerabbit@gmail.com", utf8_encode('blueRABBIT'));  //Quien envía el correo
					        $mail->AddReplyTo("interview.bluerabbit@gmail.com", utf8_encode("blueRABBIT"));  //A quien debe ir dirigida la respuesta
					        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
					        $mail->Body      = $body;
					        $mail->AltBody    = $altbody;
					        $mail->AddAddress($row->email, $row->firstName . ' ' . $row->lastName);
					        $mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
	
					        if ($mail->Send())
							{
								$response = true;
							}
							else
							{
								$response = false;
							}
						}
						
						//Create Array Response
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => 'Correo enviado exitosamente. Revisa tu Correo Electrónico'
						);
	
						//Print JSON Response
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Show Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Correo Electrónico no existe. Intenta de nuevo.'
						);
	
						////Print JSON Response
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'fields' => $fields
					);
	
					//Print JSON Response
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// resetPassword
			if ($msg == 'resetPassword' && in_array('forgotPassword', $calls))
			{
				//Read Fields
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
				
				//Check Values
				if ($iduser && $password)
				{
					//Query if the email exists already
					$query = $this->db->query("SELECT * FROM user WHERE iduser = '" . $iduser . "' LIMIT 1");
	
					//Check if user exists
					if ($query->num_rows() > 0)
					{
						//Read Object
						$row = $query->row();
						
						//Update Token into User
						$data['password'] = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]);;
						$this->db->where('iduser', $row->iduser);
						$this->db->update('user', $data);
						
						//Create Array Response
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => 'Contraseña actualizada exitosamente. Inicia Sesión.'
						);
	
						//Print JSON Response
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Show Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'El usuario no esta registrado.'
						);
	
						////Print JSON Response
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'fields' => $fields
					);
	
					//Print JSON Response
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// checkReset
			if ($msg == 'checkReset' && in_array('checkReset', $calls))
			{
				//Read Fields
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				$token = (isset($fields['token'])) ? (string)trim($fields['token']) : '';
				
				//Check Values
				if ($iduser && $token)
				{
					//Query if the email exists already
					$query = $this->db->query("SELECT * FROM user WHERE iduser = " . $iduser . " LIMIT 1");
	
					//Check if user exists
					if ($query->num_rows() > 0)
					{
						//Read Object
						$row = $query->row();
						
						//Check Token
						if ($row->token == $token)
						{
							//Create Array Response
							$array = array(
								'status' => (int)1,
								'msg' => 'success'
							);
		
							//Print JSON Response
							$this->printJSON($array);
							$output = TRUE;
						}
						else
						{
							//Show Error
							$array = array(
								'status' => (int)0,
								'msg' => 'Este token no es válido o no existe, inicia el proceso de nuevo.'
							);
		
							//Print JSON Response
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Show Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Este usuario no esta registrado.'
						);
	
						////Print JSON Response
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'fields' => $fields
					);
	
					//Print JSON Response
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// twitterLogin
			if ($msg == 'twitterLogin' && in_array('twitterLogin', $calls))
			{
				//Read Fields
				$id = (isset($fields['id'])) ? (string)trim($fields['id']) : '';
				$first_name = (isset($fields['first_name'])) ? (string)trim($fields['first_name']) : '';
				$last_name = (isset($fields['last_name'])) ? (string)trim($fields['last_name']) : '';
				$screen_name = (isset($fields['screen_name'])) ? (string)trim($fields['screen_name']) : '';
				$avatar = (isset($fields['avatar'])) ? (string)trim($fields['avatar']) : '';
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				$language = (isset($fields['language'])) ? (string)trim($fields['language']) : 'es_MX';
				$token = (isset($fields['token'])) ? (string)trim($fields['token']) : '';
				$idprofile = 3;
				$iduser = '';
				
				//Check Values
				if ($id && $first_name && $screen_name && $avatar && $email && $token)
				{
					//Query if the email exists already
					$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' LIMIT 1");
					
					//Query Profile
					$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $idprofile . " AND status = 1");
					$row_profile = $query_profile->row();
	
					//Check if user exists
					if ($query->num_rows() == 0)
					{
						//Save data into users
						$data = array(
							'firstName' => $first_name,
							'lastName' => $last_name,
							'email' => $email,
							'password' => password_hash($id, PASSWORD_DEFAULT, ['cost' => 12]),
							'avatar' => $avatar,
							'language' => $language,
							'twId' => $id,
							'twToken' => $token,
						    'idprofile' => $idprofile,
						    'createdAt' => date('Y-m-d H:i:s'),
						    'status' => 1
						);
						$this->db->insert('user', $data);
						$iduser = $this->db->insert_id();
						
						//Generate Data to send by Mail
						$data = array(
							'iduser' => $iduser,
							'email' => $email,
							'password' => $id,
							'firstName' => $first_name,
							'lastName' => $last_name,
							'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name
						);
						
						//Send Welcome Mail to blueRABBIT
						$subject = 'Welcome to blueRABBIT';
						$body = $this->load->view('mail/createUser', $data, TRUE);
						$altbody = strip_tags($body);
	
						//Check Data to send Mail
						if ($subject != '' && $body != '' && $altbody != '' && $email != '')
						{
							//Send Email
							$mail = new PHPMailer(true);
							$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
							$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
							$mail->CharSet = 'UTF-8';
							$mail->Encoding = "quoted-printable";
					        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
					        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
					        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
					        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
					        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
					        $mail->Username   = "AKIAJK4S2TODUTBKQ65Q";  // la cuenta de correo GMail
					        $mail->Password   = "AlXW+LESj55ocLzrg5pZS8dqlQmIYSH/QZaft8uPCmOl";            // password de la cuenta GMail
					        $mail->SMTPDebug  = 0;
					        $mail->SetFrom("interview.bluerabbit@gmail.com", utf8_encode('blueRABBIT'));  //Quien envía el correo
					        $mail->AddReplyTo("interview.bluerabbit@gmail.com", utf8_encode("blueRABBIT"));  //A quien debe ir dirigida la respuesta
					        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
					        $mail->Body      = $body;
					        $mail->AltBody    = $altbody;
					        $mail->AddAddress($email, $first_name . ' ' . $last_name);
					        $mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
	
					        if ($mail->Send())
							{
								$response = true;
							}
							else
							{
								$response = false;
							}
						}
					}
					else
					{
						//Read Object
						$row = $query->row();
						$iduser = $row->iduser;
						
						//Update Data
						$data = array(
							'avatar' => $avatar,
							'twId' => $id,
							'twToken' => $token,
						    'updatedAt' => date('Y-m-d H:i:s'),
						    'status' => 1
						);
						$this->db->where('iduser', $row->iduser);
						$this->db->update('user', $data);
					}
					
					//Generate Array to Respond via JSON
					$data_result = array(
						'iduser' => $iduser,
						'firstName' => $first_name,
						'lastName' => $last_name,
						'email' => $email,
						'avatar' => $avatar,
						'idprofile' => $row_profile->idprofile,
					    'profile' => $row_profile->name
					);
					
					//Create Array Response
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data_result
					);

					//Print JSON Response
					$this->printJSON($array);
					$output = TRUE;
				} 
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'fields' => $fields
					);
	
					//Print JSON Response
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// googleLogin
			if ($msg == 'googleLogin' && in_array('googleLogin', $calls))
			{
				//Read Fields
				$id = (isset($fields['id'])) ? (string)trim($fields['id']) : '';
				$first_name = (isset($fields['first_name'])) ? (string)trim($fields['first_name']) : '';
				$last_name = (isset($fields['last_name'])) ? (string)trim($fields['last_name']) : '';
				$avatar = (isset($fields['avatar'])) ? (string)trim($fields['avatar']) : '';
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				$language = (isset($fields['language'])) ? (string)trim($fields['language']) : 'es_MX';
				$token = (isset($fields['token'])) ? (string)trim($fields['token']) : '';
				$idprofile = 3;
				$iduser = '';
				
				//Check Values
				if ($id && $first_name && $avatar && $email && $token)
				{
					//Query if the email exists already
					$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' LIMIT 1");
					
					//Query Profile
					$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $idprofile . " AND status = 1");
					$row_profile = $query_profile->row();
	
					//Check if user exists
					if ($query->num_rows() == 0)
					{
						//Save data into users
						$data = array(
							'firstName' => $first_name,
							'lastName' => $last_name,
							'email' => $email,
							'password' => password_hash($id, PASSWORD_DEFAULT, ['cost' => 12]),
							'avatar' => $avatar,
							'language' => $language,
							'gaId' => $id,
							'gaToken' => $token,
						    'idprofile' => $idprofile,
						    'createdAt' => date('Y-m-d H:i:s'),
						    'status' => 1
						);
						$this->db->insert('user', $data);
						$iduser = $this->db->insert_id();
						
						//Generate Data to send by Mail
						$data = array(
							'iduser' => $iduser,
							'email' => $email,
							'password' => $id,
							'firstName' => $first_name,
							'lastName' => $last_name,
							'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name
						);
						
						//Send Welcome Mail to blueRABBIT
						$subject = 'Welcome to blueRABBIT';
						$body = $this->load->view('mail/createUser', $data, TRUE);
						$altbody = strip_tags($body);
	
						//Check Data to send Mail
						if ($subject != '' && $body != '' && $altbody != '' && $email != '')
						{
							//Send Email
							$mail = new PHPMailer(true);
							$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
							$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
							$mail->CharSet = 'UTF-8';
							$mail->Encoding = "quoted-printable";
					        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
					        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
					        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
					        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
					        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
					        $mail->Username   = "AKIAJK4S2TODUTBKQ65Q";  // la cuenta de correo GMail
					        $mail->Password   = "AlXW+LESj55ocLzrg5pZS8dqlQmIYSH/QZaft8uPCmOl";            // password de la cuenta GMail
					        $mail->SMTPDebug  = 0;
					        $mail->SetFrom("interview.bluerabbit@gmail.com", utf8_encode('blueRABBIT'));  //Quien envía el correo
					        $mail->AddReplyTo("interview.bluerabbit@gmail.com", utf8_encode("blueRABBIT"));  //A quien debe ir dirigida la respuesta
					        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
					        $mail->Body      = $body;
					        $mail->AltBody    = $altbody;
					        $mail->AddAddress($email, $first_name . ' ' . $last_name);
					        $mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
	
					        if ($mail->Send())
							{
								$response = true;
							}
							else
							{
								$response = false;
							}
						}
					}
					else
					{
						//Read Object
						$row = $query->row();
						$iduser = $row->iduser;
						
						//Update Data
						$data = array(
							'avatar' => $avatar,
							'gaId' => $id,
							'gaToken' => $token,
						    'updatedAt' => date('Y-m-d H:i:s'),
						    'status' => 1
						);
						$this->db->where('iduser', $row->iduser);
						$this->db->update('user', $data);
					}
					
					//Generate Array to Respond via JSON
					$data_result = array(
						'iduser' => $iduser,
						'firstName' => $first_name,
						'lastName' => $last_name,
						'email' => $email,
						'avatar' => $avatar,
						'idprofile' => $row_profile->idprofile,
					    'profile' => $row_profile->name
					);
					
					//Create Array Response
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data_result
					);

					//Print JSON Response
					$this->printJSON($array);
					$output = TRUE;
				} 
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'fields' => $fields
					);
	
					//Print JSON Response
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			//Save Log
			$this->saveLog($msg,$fields,$app,$array, $origin, $ip);
		}
		else
		{
			//Show Error
			$array = array(
				'status' => (int)0,
				'msg' => (string)'App not registered. Register your app to use the API.',
				'origin' => $origin,
				'app' => $app,
				'apikey' => $apikey
			);

			//Print JSON Response
			$this->printJSON($array);
			$output = TRUE;
			
			//Save Log
			$this->saveLog($msg,$fields,$app,$array,$origin,$ip);
		}
		
		
		if (!$output)
		{
			//Show Error
			$array = array(
				'status' => (int)0,
				'msg' => (string)'Invalid Call.'
			);

			//Print JSON Response
			$this->printJSON($array);
			$output = TRUE;
			
			//Save Log
			$this->saveLog($msg,$fields,$app,$array,$origin,$ip);
		}
	}

	//Timeago Function 
	public function get_timeago( $timestamp )
	{
	    $time_ago = strtotime($timestamp);  
		$current_time = time();  
		$time_difference = $current_time - $time_ago;  
		$seconds = $time_difference;  
		$minutes = round($seconds / 60 );           // value 60 is seconds  
		$hours = round($seconds / 3600);           //value 3600 is 60 minutes * 60 sec  
		$days = round($seconds / 86400);          //86400 = 24 * 60 * 60;  
		$weeks = round($seconds / 604800);          // 7*24*60*60;  
		$months = round($seconds / 2629440);     //((365+365+365+365+366)/5/12)*24*60*60  
		$years = round($seconds / 31553280);     //(365+365+365+365+366)/5 * 24 * 60 * 60  
		
		if($seconds <= 60)  
		{  
			return "Justo ahora";  
		}  
		else if($minutes <=60)  
		{  
			if($minutes==1)  
			{  
				return "hace un minuto";  
			}  
			else  
			{  
				return "hace $minutes minutos";  
			}  
		}  
		else if($hours <=24)  
		{  
			if($hours==1)  
			{  
				return "hace una hora";  
			}  
			else  
			{  
				return "hace $hours horas";  
			}  
		}  
		else if($days <= 7)  
		{  
			if($days==1)  
			{  
				return "ayer";  
			}  
			else  
			{  
				return "hace $days días";  
			}  
		}  
		else if($weeks <= 4.3) //4.3 == 52/12  
		{  
			if($weeks==1)  
			{  
				return "hace una semana";  
			}  
			else  
			{  
				return "hace $weeks semanas";  
			}  
		}  
		else if($months <=12)  
		{  
			if($months==1)  
			{  
				return "hace un mes";  
			}  
			else  
			{  
				return "hace $months meses";  
			}  
		}  
		else  
		{  
			if($years==1)  
			{  
				return "hace un año";  
			}  
			else  
			{  
				return "hace $years años";  
			}  
		}  
	}
	
	//Save Log Function
	public function saveLog( $msg,$fields,$app,$response,$origin,$ip )
	{
		//Check All Fields
		if ( $msg && $fields && $app && $response)
		{
			//Insert Log into Database
			$data = array(
				'msg' => $msg,
				'fields' => json_encode($fields),
				'app' => $app,
				'response' => json_encode($response),
				'origin' => $origin,
				'ip' => $ip,
				'createdAt' => date('Y-m-d H:i:s'),
			    'status' => 1
			);
			$this->db->insert('log', $data);
		}
	}
}