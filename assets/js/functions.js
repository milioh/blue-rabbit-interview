
	$( document ).ready(function() {
    	
    	//Read Values
    	var base_url = $('#base_url').val();
    	
    	//DatePicker Language
    	$.datepicker.regional['es'] = {
			closeText: 'Cerrar',
			prevText: '< Ant',
			nextText: 'Sig >',
			currentText: 'Hoy',
			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
			dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
			dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
			dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
			weekHeader: 'Sm',
			dateFormat: 'dd/mm/yy',
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''
		};
		
		$.datepicker.setDefaults($.datepicker.regional['es']);
    	
    	//DatePicker
    	$( "#the_player_brithdate" ).datepicker({
	    	changeYear: true,
	    	yearRange: "-100:+0",
	    	changeMonth: true,
	    	altField: "#actualDate",
	    	altFormat: "yy-mm-dd"
	    });
    	
    	//loginform Validate
    	$('#loginform').validate({
			rules: {
				inputEmail: {
					required: true,
					email: true
				},
				inputPassword: {
					required: true
				}
			}
		});
		
		//submit-loginform Click
		$('#submit-loginform').on('click', function(e) {
			e.preventDefault();
			
			//Check Valid Form
			if ($('#loginform').valid())
			{
				//Read Recaptcha Response
		    	var response = grecaptcha.getResponse();
		    	
				//Check Google Recaptcha Response
				if (response)
				{
					//Read Values
					var email = $('#login-user-name').val();
					var password = $('#login-password').val();
					var param = '{"msg": "doLogin","fields": {"email": "' + email + '", "password": "' + password + '"}}';
					
					//Disable Button
					$('#submit-loginform').prop( 'disabled', true );
					$('#submit-loginform').html('PROCESSING...');
					
					//API Call
					$.post(base_url + 'api', { param: param }).done(function( data ) {
						//Check Status Call
						if (data.status == 1)
						{
							//Reload
							location.reload();
						}
						else
						{
							//Notification
							alert(data.msg);
							
							//Enable Button
							$('#submit-loginform').prop( 'disabled', false );
			                $('#submit-loginform').html('<span class="icon icon-sign"></span>Login');
						}
					});
				}
				else
				{
					//Show Google Recaptcha Error
					alert('Demuestra que no eres un robot.');
				}
			}
			
			return false;
		});
		
		//registerform Validate
    	$('#registerform').validate({
			rules: {
				registerInputEmail: {
					required: true,
					email: true
				},
				registerInputPassword: {
					required: true
				},
				registerInputFirstName: {
					required: true
				},
				registerInputLastName: {
					required: true
				},
				registerSelectLanguage: {
					required: true
				}
			}
		});
		
		//submit-registerform Click
		$('#submit-registerform').on('click', function(e) {
			e.preventDefault();
			
			//Check Valid Form
			if ($('#registerform').valid())
			{
				//Read Values
				var email = $('#user_email').val();
				var password = $('#password').val();
				var first_name = $('#user_name').val();
				var last_name = $('#user_lastname').val();
				var language = $('#the_lang').val();
				var param = '{"msg": "createUser","fields": {"email": "' + email + '", "password": "' + password + '", "first_name": "' + first_name + '", "last_name": "' + last_name + '", "language": "' + language + '"}}';
				
				//Disable Button
				$('#submit-registerform').prop( 'disabled', true );
				$('#submit-registerform').html('PROCESSING...');
				
				//API Call
				$.post(base_url + 'api', { param: param }).done(function( data ) {
					console.log(data);
					//Check Status Call
					if (data.status == 1)
					{
						//Reload
						location.reload();
					}
					else
					{
						//Enable Button
						$('#submit-registerform').prop( 'disabled', false );
		                $('#submit-registerform').html('<span class="icon icon-sign"></span>Register New Player');
					}
				});
			}
			
			return false;
		});
		
		//profileform Validate
    	$('#profileform').validate({
			rules: {
				inputFirstName: {
					required: true
				},
				inputLastName: {
					required: true
				},
				inputEmail: {
					required: true,
					email: true
				},
				inputBirthDate: {
					required: true
				},
				selectGender: {
					required: true
				},
				selectLanguage: {
					required: true
				},			
				the_player_picture: {
					extension: "jpg|jpeg|png"
				}
			}
		});
		
		//submit-profileform Click
		$('#submit-profileform').on('click', function(e) {
			e.preventDefault();
			
			//Check Valid Form
			if ($('#profileform').valid())
			{
				//Submit Form
				$('#profileform').submit();
			}
			
			return false;
		});
		
		//btnUploadPicture Click
		$('#btnUploadPicture').on('click', function(e) {
			e.preventDefault();
			
			//Click File Input
			$('#the_player_picture').trigger('click');
			
			return false;
		});
		
		//the_player_picture on change
		$("#the_player_picture").on('change', function (e) {
			//Read File Name
			var fileName = $(this).val();
			//Extract Extension File
			var extension = fileName.replace(/^.*\./, '');
			//Check if Filename exists
			if (extension == fileName) { extension = ''; } 
	        else { extension = extension.toLowerCase(); }
	        //Check if the file is an image
	        switch (extension) {
	            case 'jpg':
	            case 'jpeg':
	            case 'png':
	            	readURL(this); break;
	            default: alert('El archivo no es válido.'); break;
	        }
	        
    	});
		
		//forgotform Validate
    	$('#forgotform').validate({
			rules: {
				inputEmail: {
					required: true,
					email: true
				}
			}
		});
		
		//submit-forgotform Click
		$('#submit-forgotform').on('click', function(e) {
			e.preventDefault();
			
			//Check Valid Form
			if ($('#forgotform').valid())
			{
				//Read Values
				var email = $('#forgot-user-name').val();
				var param = '{"msg": "forgotPassword","fields": {"email": "' + email + '"}}';
				
				//Disable Button
				$('#submit-forgotform').prop( 'disabled', true );
				$('#submit-forgotform').html('PROCESSING...');
				
				//API Call
				$.post(base_url + 'api', { param: param }).done(function( data ) {
					//Check Status Call
					if (data.status == 1)
					{
						//Notification
						alert(data.data);
						
						//Clear Form
						$('#forgotform').trigger("reset");
					}
					else
					{
						//Notification
						alert(data.msg);
					}
					
					//Enable Button
					$('#submit-forgotform').prop( 'disabled', false );
	                $('#submit-forgotform').html('Reset Password');
				});
			}
			
			return false;
		});
		
		//resetform Validate
		$('#resetform').validate({
			rules: {
				inputNewPassword: {
					required: true
				},
				inputNewConfirmPassword: {
					required: true,
					equalTo: "#reset-new-password"
				},
			}
		});
		
		//submit-resetform Click
		$('#submit-resetform').on('click', function(e) {
			e.preventDefault();
			
			//Check Valid Form
			if ($('#resetform').valid())
			{
				//Read Values
				var iduser = $('#iduser').val();
				var password = $('#reset-new-password').val();
				var param = '{"msg": "resetPassword","fields": {"iduser": "' + iduser + '", "password": "' + password + '"}}';
				
				//Disable Button
				$('#submit-resetform').prop( 'disabled', true );
				$('#submit-resetform').html('PROCESSING...');
				
				//API Call
				$.post(base_url + 'api', { param: param }).done(function( data ) {
					//Notification
					alert(data.data);
						
					//Clear Form
					$('#resetform').trigger("reset");
					
					//Enable Button
					$('#submit-resetform').prop( 'disabled', false );
	                $('#submit-resetform').html('Change Password');
				});
			}
			
			return false;
		});
		
		//btnTwitterLogin Click
		$('#btnTwitterLogin').on('click', function(e) {
			e.preventDefault();
			
			//Redirect to Login Twitter
			window.location.href = base_url + 'proccess/login_twitter';
			
			return false;
		});
		
		//btnGoogleLogin Click
		$('#btnGoogleLogin').on('click', function(e) {
			e.preventDefault();
			
			//Redirect to Login Twitter
			window.location.href = base_url + 'proccess/login_google';
			
			return false;
		});
		
		//btnTwitterConnect Click
		$('#btnTwitterConnect').on('click', function(e) {
			e.preventDefault();
			
			//Redirect to Login Twitter
			window.location.href = base_url + 'proccess/connect_twitter';
			
			return false;
		});
		
		//btnGoogleConnect Click
		$('#btnGoogleConnect').on('click', function(e) {
			e.preventDefault();
			
			//Redirect to Login Twitter
			window.location.href = base_url + 'proccess/connect_google';
			
			return false;
		});
    	
    	//readURL function
    	function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function(e) {
					$('#the_player_picture_thumb').css('background-image', 'url(' + e.target.result + ')');
    			}
				reader.readAsDataURL(input.files[0]);
			}
		}
    	
	});