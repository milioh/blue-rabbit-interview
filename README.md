## Synopsis

Interview blueRABBIT

## Code Example

La página web esta en:

* [Interview blueRABBIT](http://dev.bluerabbit.io/interview/)

## Tasks

* El usuario tiene que poder loggearse con su correo electrónico y contraseña.
* Agregar reCaptcha de Google
* Debe poder registrarse con Twitter y con Google Apps
* Debe poder recuperar contraseña (no hay pantallas, hay que armarlas con los archivos que te damos)
* Una vez adentro, debe poder actualizar su perfil. 
* Si el usuario no tiene registrado una cuenta de Twitter y/o Google Apps debe poder vincularla y que todos queden con un solo ID de usuario.
* El campo de fecha de nacimiento debe seleccionarse de jquery UI
* Debe poder cambiar contraseña
* La contraseña tiene que estar encriptada.

## Comments

El reCaptcha esta en el login, creo que debería de ir en el registro. Intente poner 2 reCaptcha en la misma pantalla, y se rompió mi avance, si es posible, pero tardaría más en habilitarlo.

Para el login de Twitter ocupe una app anterior que ya no estaba usando, ya que Twitter ahora válida las apps antes de dejarte usarlas en producción. El SDK de Twitter que ocupe se llama [TwitterOAuth](https://twitteroauth.com), si aparte del login vamos a ocupar otras funciones de Twitter, habría que revisar si son compatibles. Lo instale vía Composer.

Para el login de Google, cree una nueva app para el Single Sign On, y instale la librería vía Composer, sin embargo, me costo trabajo hacerla funcionar, ya que no encontré un ejemplo que se acercará a la forma en que hice el proyecto, por lo que use llamadas CURL para generar las llamadas a la API y generar la URL de OAuth y para obtener la info del usuario. Es compatible tanto con cuentas de Gmail como con cuentas de Google Suite.

Genere las pantallas de [Forgot](http://dev.bluerabbit.io/interview/forgot) y Reset (aquí se genera una validación con el id del usuario y un token que se generá cuando se solicita el cambio de contraseña, por lo que no te puedo generar una url para revisar el formulario, tienes que crear una cuenta y después solicitar el cambio de contraseña).

Para el Registro y Forgot, cree dos plantillas de correo usando la herramienta [Topol](https://topol.io), que puedes visualizar en los siguientes links:

* [Register](http://dev.bluerabbit.io/interview/mail/createUser)
* [Forgot](http://dev.bluerabbit.io/interview/mail/forgotPassword)

Por cierto, los correos llegan con el mail "interview.bluerabbit@gmail.com", que valide con mi cuenta de Amazon SES, para poder enviar los correos sin que lleguen a SPAM y para usar la librería [PHPMailer](https://github.com/PHPMailer/PHPMailer) que es la misma que usa Wordpress.

Para la actualización del perfil hice unos pequeños cambios, el campo de "language" lo subí en el mismo row de "birthdate" y "gender" para hacerlo más homogéneo, y en la parte de abajo puse los botones para conectar Twitter y Google, en caso de que ya tengas conectada la cuenta, el botón estará deshabilitado y la parte de cambio de Contraseña no se desplegará, esto es, porque si ya estás usando tu red social para entrar al sistema, no deberías de necesitar una contraseña y menos cambiarla. Avísame si esto esta mal para volver a habilitar el cambio de Contraseña siempre aunque hayas conectado tu red social previamente.

Las imágenes de perfil, están almacenadas en un "bucket" de Amazon S3, espero que no haya problema con esto, solo habría que cambiar las credenciales de AWS más adelante. Si no las almacenaremos ahí, tendría que cambiar el proceso de upload para que se guarden en el server de la aplicación.

El campo de "birthdate" tiene 100 años hacía atrás.

Cuando no tienes vinculadas tus redes sociales, el proceso de actualización de contraseña funciona correctamente.

La contraseña esta encriptada con la función de PHP llamada [pasword_hash](http://php.net/manual/es/function.password-hash.php) y para evaluar si la contraseña es correcta en un login se usa la función de PHP llamada [password_verify](http://php.net/manual/es/function.password-verify.php).

El logout del sistema en este momento esta habilitado en la barra de navegación al dar click en el Nombre del Usuario.

Para la validación de los formularios use la librería [jQuery Validation](https://jqueryvalidation.org), donde puedes seleccionar el idioma de los textos de validación, en este momento puse los textos en español, pero creo que si deberíamos de fijar el idioma de la aplicación, porque unas cosas las encontré en español y otras en inglés.

Las aplicaciones, como te comente, siempre trato de hacerles una API JSON para que se generen las llamadas AJAX durante la ejecución de la aplicación, en este caso la API ya es compatible con otras apps quitando los problemas de [CORS](https://enable-cors.org). Cree una tabla para manejar usuarios para la API donde tengamos control de las apps que pueden ejecutar algo en la API y también limitar las llamadas que puede ejecutar, creo que esta parte fue una de las que me quito más tiempo y pude haberte entregado todo antes, sin embargo, siento que con esto podemos avanzar más rápido para los siguientes módulos.

Sobre la Base de Datos, te comparto los archivos que generé, normalmente primero creo el Diagrama Entidad Relación y después lo sincronizo con la base de datos del server, en este caso, no pude conectarme directamente, así que hice un dump de la estructura y la subí a través de PHP MyAdmin, te dejo los archivos por si los quieres checar:

* [DER usando MySQL Workbench](https://www.dropbox.com/s/5kyo06a6vf24m2m/789556_milio.mwb?dl=0)
* [Impresión del DER en PDF](https://www.dropbox.com/s/xo04f8zvmqlq4p4/789556_milio.pdf?dl=0)
* [Dump SQL con data inicial (app, profile)](https://www.dropbox.com/s/j0yplpg6inokj2j/789556_milio.sql?dl=0)

## Installation

Se debe de configurar la base de datos local en donde se instale la aplicación. El servidor debe de tener habilitada la librería "mcrypt" y "mod_rewrite".

## Contributors

* Emiliano Hernández ([@milioh](https://twitter.com/milioh))

## License

Copyright 2018 - blueRABBIT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.